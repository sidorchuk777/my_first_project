"""
Простым называется число, которое делится нацело только на единицу и само себя. Число 1 не считается простым.
Напишите программу, которая находит все простые числа в заданном промежутке, выводит их на экран,
а затем по требованию пользователя выводит их сумму либо произведение.
"""

print("Введите")
number1 = int(input("число от: "))
number2 = int(input("число до: "))
my_list = []
res = 1


for i in range(number1, number2+1):
    k = 0
    for j in range(1, i+1):
        if i % j == 0:
            k += 1
    if k == 2:
        my_list.append(i)

print("Простые числа:", my_list)
print()
print("(Нажмите '+' чтобы сумировать числа или '*' для вывода произведения)")
opt = input("Введите команду: ")

if  opt == "+":
    print(sum(my_list))
if opt == "*":
    for x in range(len(my_list)):
        res *= my_list[x]
    print("Производная чисел:", res)

